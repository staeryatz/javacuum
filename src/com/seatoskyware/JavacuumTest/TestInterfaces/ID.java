package com.seatoskyware.JavacuumTest.TestInterfaces;

public interface ID {
    IA getD1();
    IB getD2();
    IC getD3();
}
