package com.seatoskyware.JavacuumTest.TestInterfaces;

import java.util.List;

public interface IDependsOnMultiple {
    List<Object> getInjections();
}
