package com.seatoskyware.JavacuumTest.TestInterfaces;

public interface IC {
    IA getD1();
    IB getD2();
}
