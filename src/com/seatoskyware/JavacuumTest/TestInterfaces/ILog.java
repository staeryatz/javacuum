package com.seatoskyware.JavacuumTest.TestInterfaces;

import java.util.List;

public interface ILog {
    void write(String value);
    List<String> getLines();
}
