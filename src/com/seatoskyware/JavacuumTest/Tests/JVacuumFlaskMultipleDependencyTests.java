package com.seatoskyware.JavacuumTest.Tests;

import com.seatoskyware.Javacuum.Depends;
import com.seatoskyware.Javacuum.JVacuumFlask;
import com.seatoskyware.JavacuumTest.TestClasses.DependsOnMultiple;
import com.seatoskyware.JavacuumTest.TestInterfaces.IDependsOnMultiple;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

/**
 * @author Jeffrey Bakker
 */
public class JVacuumFlaskMultipleDependencyTests {

    @BeforeMethod
    public void setUp() throws Exception {

    }

    @AfterMethod
    public void tearDown() throws Exception {

    }

    @Test
    public void testRegisterAbstractAndConcreteAndResolveDifferentObjects() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, () -> "Injected");
        container.registerComponent(Integer.class, () -> 1000);

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(String.class, Integer.class),
                (s, i) -> new DependsOnMultiple((String) s, (Integer) i));

        container.registerComponent(DependsOnMultiple.class,
                Depends.on(String.class, Integer.class),
                (s, i) -> new DependsOnMultiple((String) s, (Integer) i));

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 2);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof Integer);

        Object concreteObject = container.resolveComponent(DependsOnMultiple.class);
        Assert.assertNotNull(concreteObject);
        Assert.assertTrue(concreteObject instanceof DependsOnMultiple);

        List<Object> concreteDepends = ((DependsOnMultiple)concreteObject).getInjections();
        Assert.assertEquals(concreteDepends.size(), 2);
        Object concreteInjected1 = concreteDepends.get(0);
        Object concreteInjected2 = concreteDepends.get(1);
        Assert.assertTrue(concreteInjected1 instanceof String);
        Assert.assertTrue(concreteInjected2 instanceof Integer);

        Assert.assertTrue(concreteInjected1.equals(abstractInjected1));
        Assert.assertTrue(concreteInjected2.equals(abstractInjected2));
    }

    @Test
    public void testResolveDependsOn2() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, () -> "Injected");
        container.registerComponent(Integer.class, () -> 1000);

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(String.class, Integer.class),
                (s, i) -> new DependsOnMultiple((String) s, (Integer) i));

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 2);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof Integer);
    }


    @Test
    public void testResolveDependsOn3() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, () -> "Injected");

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(String.class, String.class, String.class),
                (d1, d2, d3) -> new DependsOnMultiple(d1, d2, d3));

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 3);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Object abstractInjected3 = abstractDepends.get(2);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof String);
        Assert.assertTrue(abstractInjected3 instanceof String);
    }

    @Test
    public void testResolveDependsOn4() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, () -> "Injected");

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(String.class, String.class, String.class, String.class),
                (d1, d2, d3, d4) -> new DependsOnMultiple(d1, d2, d3, d4));

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 4);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Object abstractInjected3 = abstractDepends.get(2);
        Object abstractInjected4 = abstractDepends.get(3);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof String);
        Assert.assertTrue(abstractInjected3 instanceof String);
        Assert.assertTrue(abstractInjected4 instanceof String);
    }

    @Test
    public void testResolveDependsOn5() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, () -> "Injected");

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(String.class, String.class, String.class, String.class, String.class),
                (d1, d2, d3, d4, d5) -> new DependsOnMultiple(d1, d2, d3, d4, d5));

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 5);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Object abstractInjected3 = abstractDepends.get(2);
        Object abstractInjected4 = abstractDepends.get(3);
        Object abstractInjected5 = abstractDepends.get(4);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof String);
        Assert.assertTrue(abstractInjected3 instanceof String);
        Assert.assertTrue(abstractInjected4 instanceof String);
        Assert.assertTrue(abstractInjected5 instanceof String);
    }

    @Test
    public void testResolveDependsOn6() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, () -> "Injected");

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(
                        String.class, String.class, String.class,
                        String.class, String.class, String.class),
                (d1, d2, d3, d4, d5, d6) -> new DependsOnMultiple(d1, d2, d3, d4, d5, d6));

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 6);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Object abstractInjected3 = abstractDepends.get(2);
        Object abstractInjected4 = abstractDepends.get(3);
        Object abstractInjected5 = abstractDepends.get(4);
        Object abstractInjected6 = abstractDepends.get(5);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof String);
        Assert.assertTrue(abstractInjected3 instanceof String);
        Assert.assertTrue(abstractInjected4 instanceof String);
        Assert.assertTrue(abstractInjected5 instanceof String);
        Assert.assertTrue(abstractInjected6 instanceof String);
    }

    @Test
    public void testResolveDependsOn6Mixed() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, () -> "Injected");
        container.registerComponent(Integer.class, () -> 1000);
        container.registerComponent(Float.class, () -> 1.01f);
        container.registerComponent(Double.class, () -> 2.02);
        container.registerComponent(Byte.class, () -> new Integer(1).byteValue());
        container.registerComponent(Boolean.class, () -> true);

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(
                        String.class, Integer.class, Float.class,
                        Double.class, Byte.class, Boolean.class),
                (d1, d2, d3, d4, d5, d6) -> new DependsOnMultiple(d1, d2, d3, d4, d5, d6));

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 6);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Object abstractInjected3 = abstractDepends.get(2);
        Object abstractInjected4 = abstractDepends.get(3);
        Object abstractInjected5 = abstractDepends.get(4);
        Object abstractInjected6 = abstractDepends.get(5);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof Integer);
        Assert.assertTrue(abstractInjected3 instanceof Float);
        Assert.assertTrue(abstractInjected4 instanceof Double);
        Assert.assertTrue(abstractInjected5 instanceof Byte);
        Assert.assertTrue(abstractInjected6 instanceof Boolean);
    }

    @Test
    public void testResolveDependsOn6MixedShared() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, () -> "Injected");
        container.registerComponent(Integer.class, () -> 1000);
        container.registerComponent(Float.class, () -> 1.01f);

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(
                        String.class, Integer.class, Float.class,
                        String.class, Integer.class, Float.class),
                (d1, d2, d3, d4, d5, d6) -> new DependsOnMultiple(d1, d2, d3, d4, d5, d6));

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 6);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Object abstractInjected3 = abstractDepends.get(2);
        Object abstractInjected4 = abstractDepends.get(3);
        Object abstractInjected5 = abstractDepends.get(4);
        Object abstractInjected6 = abstractDepends.get(5);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof Integer);
        Assert.assertTrue(abstractInjected3 instanceof Float);
        Assert.assertTrue(abstractInjected4 instanceof String);
        Assert.assertTrue(abstractInjected5 instanceof Integer);
        Assert.assertTrue(abstractInjected6 instanceof Float);
    }
}
