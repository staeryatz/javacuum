package com.seatoskyware.JavacuumTest.Tests;

import com.seatoskyware.Javacuum.Depends;
import com.seatoskyware.Javacuum.JVacuumFlask;
import com.seatoskyware.JavacuumTest.TestClasses.ArrayLog;
import com.seatoskyware.JavacuumTest.TestClasses.StartupLoggerA;
import com.seatoskyware.JavacuumTest.TestInterfaces.ILog;
import com.seatoskyware.JavacuumTest.TestInterfaces.ILoggerA;
import com.sun.javaws.exceptions.InvalidArgumentException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author Jeffrey Bakker
 */
public class JVacuumFlaskRegistrationConflictTests {

    @BeforeMethod
    public void setUp() throws Exception {

    }

    @AfterMethod
    public void tearDown() throws Exception {

    }

    @Test
    public void testRegisterComponentSameInterfaceTwiceThrows() throws Exception {

        String exceptionMessageExpected = "Cannot register the same type twice.";
        String exceptionMessageActual = null;

        JVacuumFlask container = new JVacuumFlask();

        ILog log = new ArrayLog();
        container.registerComponent(ILog.class, () -> log);

        try {
            container.registerComponent(ILog.class, () -> log);
        } catch (InvalidArgumentException e) {
            exceptionMessageActual = e.getMessage();
        }

        Assert.assertNotNull(exceptionMessageActual);
        Assert.assertTrue(exceptionMessageActual.contains(exceptionMessageExpected));
    }

    @Test
    public void testRegisterComponentSameClassTwiceThrows() throws Exception {

        String exceptionMessageExpected = "Cannot register the same type twice.";
        String exceptionMessageActual = null;

        JVacuumFlask container = new JVacuumFlask();

        ILog log = new ArrayLog();
        container.registerComponent(ArrayLog.class, () -> log);

        try {
            container.registerComponent(ArrayLog.class, () -> log);
        } catch (InvalidArgumentException e) {
            exceptionMessageActual = e.getMessage();
        }

        Assert.assertNotNull(exceptionMessageActual);
        Assert.assertTrue(exceptionMessageActual.contains(exceptionMessageExpected));
    }

    @Test
    public void testRegisterComponentSameInterfaceTwiceWithDependenciesThrows() throws Exception {

        String exceptionMessageExpected = "Cannot register the same type twice.";
        String exceptionMessageActual = null;

        JVacuumFlask container = new JVacuumFlask();

        ILog log = new ArrayLog();
        container.registerComponent(ILog.class, () -> log);

        container.registerComponent(ILoggerA.class,
                Depends.on(ILog.class), (l) -> new StartupLoggerA((ILog) l));

        try {
            container.registerComponent(ILoggerA.class,
                    Depends.on(ILog.class), (l) -> new StartupLoggerA((ILog) l));
        } catch (InvalidArgumentException e) {
            exceptionMessageActual = e.getMessage();
        }

        Assert.assertNotNull(exceptionMessageActual);
        Assert.assertTrue(exceptionMessageActual.contains(exceptionMessageExpected));
    }

    @Test
    public void testRegisterComponentSameInterfaceAsDependency() throws Exception {

        String exceptionMessageExpected = "Dependency type cannot be same type as component type.";
        String exceptionMessageActual = null;

        JVacuumFlask container = new JVacuumFlask();

        ILog log = new ArrayLog();
        container.registerComponent(ILog.class, () -> log);

        try {
            container.registerComponent(ILoggerA.class,
                    Depends.on(ILoggerA.class), (l) -> null);
        } catch (InvalidArgumentException e) {
            exceptionMessageActual = e.getMessage();
        }

        Assert.assertNotNull(exceptionMessageActual);
        Assert.assertTrue(exceptionMessageActual.contains(exceptionMessageExpected));
    }

    @Test
    public void testRegisterComponentSameClassAsDependency() throws Exception {

        String exceptionMessageExpected = "Dependency type cannot be same type as component type.";
        String exceptionMessageActual = null;

        JVacuumFlask container = new JVacuumFlask();

        ILog log = new ArrayLog();
        container.registerComponent(ILog.class, () -> log);

        try {
            container.registerComponent(StartupLoggerA.class,
                    Depends.on(StartupLoggerA.class), (l) -> null);
        } catch (InvalidArgumentException e) {
            exceptionMessageActual = e.getMessage();
        }

        Assert.assertNotNull(exceptionMessageActual);
        Assert.assertTrue(exceptionMessageActual.contains(exceptionMessageExpected));
    }
}
