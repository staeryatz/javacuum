package com.seatoskyware.JavacuumTest.Tests;

import com.seatoskyware.Javacuum.Depends;
import com.seatoskyware.Javacuum.Initialization.*;
import com.seatoskyware.Javacuum.JVacuumFlask;
import com.seatoskyware.JavacuumTest.TestClasses.DependsOnMultiple;
import com.seatoskyware.JavacuumTest.TestInterfaces.IDependsOnMultiple;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

/**
 * @author Jeffrey Bakker
 */
public class JVacuumFlaskMultipleDependencyFactoryTests {

    @BeforeMethod
    public void setUp() throws Exception {

    }

    @AfterMethod
    public void tearDown() throws Exception {

    }

    class stringFactory implements Initializer {
        public Object initialize() {
            return "Injected";
        }
    }

    class integerFactory implements Initializer {
        public Object initialize() {
            return 1000;
        }
    }

    class floatFactory implements Initializer {
        public Object initialize() {
            return 1.01f;
        }
    }

    class doubleFactory implements Initializer {
        public Object initialize() {
            return 2.02;
        }
    }

    class byteFactory implements Initializer {
        public Object initialize() {
            return new Integer(1).byteValue();
        }
    }

    class booleanFactory implements Initializer {
        public Object initialize() {
            return true;
        }
    }

    class dependsOn2Factory implements Initializer2 {
        public Object initialize(Object d1, Object d2) {
            return new DependsOnMultiple(d1, d2);
        }
    }

    class dependsOn3Factory implements Initializer3 {
        public Object initialize(Object d1, Object d2, Object d3) {
            return new DependsOnMultiple(d1, d2, d3);
        }
    }

    class dependsOn4Factory implements Initializer4 {
        public Object initialize(Object d1, Object d2, Object d3, Object d4) {
            return new DependsOnMultiple(d1, d2, d3, d4);
        }
    }

    class dependsOn5Factory implements Initializer5 {
        public Object initialize(Object d1, Object d2, Object d3, Object d4, Object d5) {
            return new DependsOnMultiple(d1, d2, d3, d4, d5);
        }
    }

    class dependsOn6Factory implements Initializer6 {
        public Object initialize(Object d1, Object d2, Object d3,
                                 Object d4, Object d5, Object d6) {
            return new DependsOnMultiple(d1, d2, d3, d4, d5, d6);
        }
    }

    class dependsOnVa12Factory implements InitializerVa {
        public Object initialize(Object... deps) {
            return new DependsOnMultiple(
                    deps[0], deps[1], deps[2], deps[3], deps[4], deps[5],
                    deps[6], deps[7], deps[8], deps[9], deps[10], deps[11]);
        }
    }

    @Test
    public void testRegisterAbstractAndConcreteAndResolveDifferentObjects() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, new stringFactory());
        container.registerComponent(Integer.class, new integerFactory());

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(String.class, Integer.class),
                new dependsOn2Factory());

        container.registerComponent(DependsOnMultiple.class,
                Depends.on(String.class, Integer.class),
                new dependsOn2Factory());

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 2);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof Integer);

        Object concreteObject = container.resolveComponent(DependsOnMultiple.class);
        Assert.assertNotNull(concreteObject);
        Assert.assertTrue(concreteObject instanceof DependsOnMultiple);

        List<Object> concreteDepends = ((DependsOnMultiple)concreteObject).getInjections();
        Assert.assertEquals(concreteDepends.size(), 2);
        Object concreteInjected1 = concreteDepends.get(0);
        Object concreteInjected2 = concreteDepends.get(1);
        Assert.assertTrue(concreteInjected1 instanceof String);
        Assert.assertTrue(concreteInjected2 instanceof Integer);

        Assert.assertTrue(concreteInjected1.equals(abstractInjected1));
        Assert.assertTrue(concreteInjected2.equals(abstractInjected2));
    }

    @Test
    public void testResolveDependsOn2() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, new stringFactory());
        container.registerComponent(Integer.class, new integerFactory());

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(String.class, Integer.class),
                new dependsOn2Factory());

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 2);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof Integer);
    }


    @Test
    public void testResolveDependsOn3() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, new stringFactory());

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(String.class, String.class, String.class),
                new dependsOn3Factory());

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 3);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Object abstractInjected3 = abstractDepends.get(2);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof String);
        Assert.assertTrue(abstractInjected3 instanceof String);
    }

    @Test
    public void testResolveDependsOn4() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, new stringFactory());

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(String.class, String.class, String.class, String.class),
                new dependsOn4Factory());

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 4);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Object abstractInjected3 = abstractDepends.get(2);
        Object abstractInjected4 = abstractDepends.get(3);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof String);
        Assert.assertTrue(abstractInjected3 instanceof String);
        Assert.assertTrue(abstractInjected4 instanceof String);
    }

    @Test
    public void testResolveDependsOn5() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, new stringFactory());

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(String.class, String.class, String.class, String.class, String.class),
                new dependsOn5Factory());

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 5);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Object abstractInjected3 = abstractDepends.get(2);
        Object abstractInjected4 = abstractDepends.get(3);
        Object abstractInjected5 = abstractDepends.get(4);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof String);
        Assert.assertTrue(abstractInjected3 instanceof String);
        Assert.assertTrue(abstractInjected4 instanceof String);
        Assert.assertTrue(abstractInjected5 instanceof String);
    }

    @Test
    public void testResolveDependsOn6() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, new stringFactory());

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(
                        String.class, String.class, String.class,
                        String.class, String.class, String.class),
                new dependsOn6Factory());

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 6);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Object abstractInjected3 = abstractDepends.get(2);
        Object abstractInjected4 = abstractDepends.get(3);
        Object abstractInjected5 = abstractDepends.get(4);
        Object abstractInjected6 = abstractDepends.get(5);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof String);
        Assert.assertTrue(abstractInjected3 instanceof String);
        Assert.assertTrue(abstractInjected4 instanceof String);
        Assert.assertTrue(abstractInjected5 instanceof String);
        Assert.assertTrue(abstractInjected6 instanceof String);
    }

    @Test
    public void testResolveDependsOn6Mixed() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, new stringFactory());
        container.registerComponent(Integer.class, new integerFactory());
        container.registerComponent(Float.class, new floatFactory());
        container.registerComponent(Double.class, new doubleFactory());
        container.registerComponent(Byte.class, new byteFactory());
        container.registerComponent(Boolean.class, new booleanFactory());

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(
                        String.class, Integer.class, Float.class,
                        Double.class, Byte.class, Boolean.class),
                new dependsOn6Factory());

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 6);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Object abstractInjected3 = abstractDepends.get(2);
        Object abstractInjected4 = abstractDepends.get(3);
        Object abstractInjected5 = abstractDepends.get(4);
        Object abstractInjected6 = abstractDepends.get(5);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof Integer);
        Assert.assertTrue(abstractInjected3 instanceof Float);
        Assert.assertTrue(abstractInjected4 instanceof Double);
        Assert.assertTrue(abstractInjected5 instanceof Byte);
        Assert.assertTrue(abstractInjected6 instanceof Boolean);
    }

    @Test
    public void testResolveDependsOn6MixedShared() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, new stringFactory());
        container.registerComponent(Integer.class, new integerFactory());
        container.registerComponent(Float.class, new floatFactory());

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(
                        String.class, Integer.class, Float.class,
                        String.class, Integer.class, Float.class),
                new dependsOn6Factory());

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 6);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Object abstractInjected3 = abstractDepends.get(2);
        Object abstractInjected4 = abstractDepends.get(3);
        Object abstractInjected5 = abstractDepends.get(4);
        Object abstractInjected6 = abstractDepends.get(5);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof Integer);
        Assert.assertTrue(abstractInjected3 instanceof Float);
        Assert.assertTrue(abstractInjected4 instanceof String);
        Assert.assertTrue(abstractInjected5 instanceof Integer);
        Assert.assertTrue(abstractInjected6 instanceof Float);
    }

    @Test
    public void testResolveDependsOn12MixedShared() throws Exception {

        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(String.class, new stringFactory());
        container.registerComponent(Integer.class, new integerFactory());
        container.registerComponent(Float.class, new floatFactory());
        container.registerComponent(Double.class, new doubleFactory());
        container.registerComponent(Byte.class, new byteFactory());
        container.registerComponent(Boolean.class, new booleanFactory());

        container.registerComponentVa(IDependsOnMultiple.class,
                Depends.on(
                        String.class, Integer.class, Float.class,
                        Double.class, Byte.class, Boolean.class,
                        String.class, Integer.class, Float.class,
                        Double.class, Byte.class, Boolean.class),
                new dependsOnVa12Factory());

        Object abstractObject = container.resolveComponent(IDependsOnMultiple.class);
        Assert.assertNotNull(abstractObject);
        Assert.assertTrue(abstractObject instanceof IDependsOnMultiple);

        List<Object> abstractDepends = ((IDependsOnMultiple)abstractObject).getInjections();
        Assert.assertEquals(abstractDepends.size(), 12);
        Object abstractInjected1 = abstractDepends.get(0);
        Object abstractInjected2 = abstractDepends.get(1);
        Object abstractInjected3 = abstractDepends.get(2);
        Object abstractInjected4 = abstractDepends.get(3);
        Object abstractInjected5 = abstractDepends.get(4);
        Object abstractInjected6 = abstractDepends.get(5);
        Object abstractInjected7 = abstractDepends.get(6);
        Object abstractInjected8 = abstractDepends.get(7);
        Object abstractInjected9 = abstractDepends.get(8);
        Object abstractInjected10 = abstractDepends.get(9);
        Object abstractInjected11 = abstractDepends.get(10);
        Object abstractInjected12 = abstractDepends.get(11);
        Assert.assertTrue(abstractInjected1 instanceof String);
        Assert.assertTrue(abstractInjected2 instanceof Integer);
        Assert.assertTrue(abstractInjected3 instanceof Float);
        Assert.assertTrue(abstractInjected4 instanceof Double);
        Assert.assertTrue(abstractInjected5 instanceof Byte);
        Assert.assertTrue(abstractInjected6 instanceof Boolean);
        Assert.assertTrue(abstractInjected7 instanceof String);
        Assert.assertTrue(abstractInjected8 instanceof Integer);
        Assert.assertTrue(abstractInjected9 instanceof Float);
        Assert.assertTrue(abstractInjected10 instanceof Double);
        Assert.assertTrue(abstractInjected11 instanceof Byte);
        Assert.assertTrue(abstractInjected12 instanceof Boolean);
    }
}
