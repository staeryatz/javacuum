package com.seatoskyware.JavacuumTest.Tests;

import com.seatoskyware.Javacuum.Depends;
import com.seatoskyware.Javacuum.JVacuumFlask;
import com.seatoskyware.JavacuumTest.TestClasses.ClassA;
import com.seatoskyware.JavacuumTest.TestClasses.ClassB;
import com.seatoskyware.JavacuumTest.TestClasses.ClassC;
import com.seatoskyware.JavacuumTest.TestClasses.ClassD;
import com.seatoskyware.JavacuumTest.TestInterfaces.IA;
import com.seatoskyware.JavacuumTest.TestInterfaces.IB;
import com.seatoskyware.JavacuumTest.TestInterfaces.IC;
import com.seatoskyware.JavacuumTest.TestInterfaces.ID;
import com.sun.javaws.exceptions.InvalidArgumentException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author Jeffrey Bakker
 */
public class JVacuumFlaskResolveConflictTests {

    @BeforeMethod
    public void setUp() throws Exception {

    }

    @AfterMethod
    public void tearDown() throws Exception {

    }

    @Test
    public void testResolveUnregisteredClassThrows() {

        boolean didThrow = false;
        JVacuumFlask container = new JVacuumFlask();

        try {
            container.resolveComponent(String.class);
        } catch (InvalidArgumentException e) {
            didThrow = true;
        }
        Assert.assertTrue(didThrow);
    }

    @Test
    public void testRegisterAbstractResolveConcreteThrows() throws Exception {

        boolean didThrow = false;
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(IA.class, () -> new ClassA());

        try {
            container.resolveComponent(ClassA.class);
        } catch (InvalidArgumentException e) {
            didThrow = true;
        }
        Assert.assertTrue(didThrow);
    }

    @Test
    public void testRegisterConcreteResolveAbstractThrows() throws Exception {

        boolean didThrow = false;
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(ClassA.class, () -> new ClassA());

        try {
            container.resolveComponent(IA.class);
        } catch (InvalidArgumentException e) {
            didThrow = true;
        }
        Assert.assertTrue(didThrow);
    }

    @Test
    public void testRegisterConcreteImplicitResolveAbstractThrows() throws Exception {

        boolean didThrow = false;
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(new ClassA());

        try {
            container.resolveComponent(IA.class);
        } catch (InvalidArgumentException e) {
            didThrow = true;
        }
        Assert.assertTrue(didThrow);
    }
}