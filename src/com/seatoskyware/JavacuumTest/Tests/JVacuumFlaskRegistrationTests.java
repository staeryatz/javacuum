package com.seatoskyware.JavacuumTest.Tests;

import com.seatoskyware.Javacuum.Depends;
import com.seatoskyware.Javacuum.JVacuumFlask;
import com.seatoskyware.JavacuumTest.TestClasses.ClassA;
import com.seatoskyware.JavacuumTest.TestClasses.ClassB;
import com.seatoskyware.JavacuumTest.TestClasses.ClassC;
import com.seatoskyware.JavacuumTest.TestClasses.ClassD;
import com.seatoskyware.JavacuumTest.TestInterfaces.IA;
import com.seatoskyware.JavacuumTest.TestInterfaces.IB;
import com.seatoskyware.JavacuumTest.TestInterfaces.IC;
import com.seatoskyware.JavacuumTest.TestInterfaces.ID;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author Jeffrey Bakker
 */
public class JVacuumFlaskRegistrationTests {

    @BeforeMethod
    public void setUp() throws Exception {

    }

    @AfterMethod
    public void tearDown() throws Exception {

    }

    @Test
    public void testRegisterComponentWithInstance() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent("testString");
        String s = (String)container.resolveComponent(String.class);
        Assert.assertNotNull(s);
        Assert.assertEquals(s, "testString");
    }

    @Test
    public void testRegisterInterfaceNoDeps() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(IA.class, () -> new ClassA());
        IA resolved = (IA)container.resolveComponent(IA.class);
        Assert.assertNotNull(resolved);
    }

    @Test
    public void testRegisterConcreteNoDeps() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(ClassA.class, () -> new ClassA());
        ClassA resolved = (ClassA)container.resolveComponent(ClassA.class);
        Assert.assertNotNull(resolved);
    }

    @Test
    public void testRegisterInterface1Dependency() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(IA.class, () -> new ClassA());
        container.registerComponent(IB.class,
                Depends.on(IA.class),
                (d1) -> new ClassB((IA)d1));
        IB resolved = (IB)container.resolveComponent(IB.class);
        IA dep = resolved.getD1();
        Assert.assertNotNull(resolved);
        Assert.assertNotNull(dep);
    }

    @Test
    public void testRegisterConcrete1Dependency() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(ClassA.class, () -> new ClassA());
        container.registerComponent(ClassB.class,
                Depends.on(ClassA.class),
                (d1) -> new ClassB((ClassA)d1));
        ClassB resolved = (ClassB)container.resolveComponent(ClassB.class);
        IA dep = resolved.getD1();
        Assert.assertNotNull(resolved);
        Assert.assertNotNull(dep);
    }

    @Test
    public void testRegisterInterface2DependenciesNesting() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(IA.class, () -> new ClassA());
        container.registerComponent(IB.class,
                Depends.on(IA.class),
                (d1) -> new ClassB((IA)d1));
        container.registerComponent(IC.class,
                Depends.on(IA.class, IB.class),
                (d1, d2) -> new ClassC((IA)d1, (IB)d2));
        IC resolved = (IC)container.resolveComponent(IC.class);
        IA dep1 = resolved.getD1();
        IB dep2 = resolved.getD2();
        Assert.assertNotNull(resolved);
        Assert.assertNotNull(dep1);
        Assert.assertNotNull(dep2);
    }

    @Test
    public void testRegisterConcrete2DependenciesNesting() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(ClassA.class, () -> new ClassA());
        container.registerComponent(ClassB.class,
                Depends.on(ClassA.class),
                (d1) -> new ClassB((ClassA)d1));
        container.registerComponent(ClassC.class,
                Depends.on(ClassA.class, ClassB.class),
                (d1, d2) -> new ClassC((ClassA)d1, (ClassB)d2));
        ClassC resolved = (ClassC)container.resolveComponent(ClassC.class);
        IA dep1 = resolved.getD1();
        IB dep2 = resolved.getD2();
        Assert.assertNotNull(resolved);
        Assert.assertNotNull(dep1);
        Assert.assertNotNull(dep2);
    }

    @Test
    public void testRegisterInterface3DependenciesNesting() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(IA.class, () -> new ClassA());
        container.registerComponent(IB.class,
                Depends.on(IA.class),
                (d1) -> new ClassB((IA)d1));
        container.registerComponent(IC.class,
                Depends.on(IA.class, IB.class),
                (d1, d2) -> new ClassC((IA)d1, (IB)d2));
        container.registerComponent(ID.class,
                Depends.on(IA.class, IB.class, IC.class),
                (d1, d2, d3) -> new ClassD((IA)d1, (IB)d2, (IC)d3));
        ID resolved = (ID)container.resolveComponent(ID.class);
        IA dep1 = resolved.getD1();
        IB dep2 = resolved.getD2();
        IC dep3 = resolved.getD3();
        Assert.assertNotNull(resolved);
        Assert.assertNotNull(dep1);
        Assert.assertNotNull(dep2);
        Assert.assertNotNull(dep3);
    }

    @Test
    public void testRegisterConcrete3DependenciesNesting() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(ClassA.class, () -> new ClassA());
        container.registerComponent(ClassB.class,
                Depends.on(ClassA.class),
                (d1) -> new ClassB((ClassA)d1));
        container.registerComponent(ClassC.class,
                Depends.on(ClassA.class, ClassB.class),
                (d1, d2) -> new ClassC((ClassA)d1, (ClassB)d2));
        container.registerComponent(ClassD.class,
                Depends.on(ClassA.class, ClassB.class, ClassC.class),
                (d1, d2, d3) -> new ClassD((ClassA)d1, (ClassB)d2, (ClassC)d3));
        ClassD resolved = (ClassD)container.resolveComponent(ClassD.class);
        IA dep1 = resolved.getD1();
        IB dep2 = resolved.getD2();
        IC dep3 = resolved.getD3();
        Assert.assertNotNull(resolved);
        Assert.assertNotNull(dep1);
        Assert.assertNotNull(dep2);
        Assert.assertNotNull(dep3);
    }
}