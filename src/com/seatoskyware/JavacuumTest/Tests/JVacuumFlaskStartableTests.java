package com.seatoskyware.JavacuumTest.Tests;

import com.seatoskyware.Javacuum.Depends;
import com.seatoskyware.Javacuum.JVacuumFlask;
import com.seatoskyware.JavacuumTest.TestClasses.*;
import com.seatoskyware.JavacuumTest.TestInterfaces.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

/**
 * @author Jeffrey Bakker
 */
public class JVacuumFlaskStartableTests {

    @BeforeMethod
    public void setUp() throws Exception {

    }

    @AfterMethod
    public void tearDown() throws Exception {

    }

    @Test
    public void testStartableNoAutoResolve() throws Exception {

        ILog log = new ArrayLog();
        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(ILog.class, () -> log);

        container.registerComponent(ILoggerA.class,
                Depends.on(ILog.class), (l) -> new StartupLoggerA((ILog)l));

        container.registerComponent(ILoggerB.class,
                Depends.on(ILog.class), (l) -> new StartupLoggerB((ILog)l));

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(ILoggerA.class, ILoggerB.class),
                (loggerA, loggerB) -> new DependsOnMultiple(loggerA, loggerB));

        container.start(false);

        List<String> logLines = log.getLines();
        Assert.assertTrue((logLines.size() == 0), "startup log is empty.");
    }

    @Test
    public void testStartableAutoResolve() throws Exception {

        String expected1 = StartupLoggerA.class.toString();
        String expected2 = StartupLoggerB.class.toString();

        ILog log = new ArrayLog();
        JVacuumFlask container = new JVacuumFlask();

        container.registerComponent(ILog.class, () -> log);

        container.registerComponent(ILoggerA.class,
                Depends.on(ILog.class), (l) -> new StartupLoggerA((ILog)l));

        container.registerComponent(ILoggerB.class,
                Depends.on(ILog.class), (l) -> new StartupLoggerB((ILog)l));

        container.registerComponent(IDependsOnMultiple.class,
                Depends.on(ILoggerA.class, ILoggerB.class),
                (loggerA, loggerB) -> new DependsOnMultiple(loggerA, loggerB));

        container.start(true);

        List<String> logLines = log.getLines();
        Assert.assertTrue(logLines.contains(expected1));
        Assert.assertTrue(logLines.contains(expected2));
    }

//    @Test
//    public void testStartableResolveOrder1() throws Exception {
//
//        String expected1 = StartupLoggerA.class.toString();
//        String expected2 = StartupLoggerB.class.toString();
//
//        ILog log = new ArrayLog();
//        JVacuumFlask container = new JVacuumFlask();
//
//        container.registerComponent(ILog.class, () -> log);
//
//        container.registerComponent(ILoggerA.class,
//                Depends.on(ILog.class), (l) -> new StartupLoggerA((ILog)l));
//
//        container.registerComponent(ILoggerB.class,
//                Depends.on(ILog.class), (l) -> new StartupLoggerB((ILog)l));
//
//        container.registerComponent(IDependsOnMultiple.class,
//                Depends.on(ILoggerA.class, ILoggerB.class),
//                (loggerA, loggerB) -> new DependsOnMultiple(loggerA, loggerB));
//
//        Object testObject = container.resolveComponent(IDependsOnMultiple.class);
//        container.start(false);
//
//        Assert.assertNotNull(testObject, "testOject is not null.");
//        Assert.assertTrue((testObject instanceof IDependsOnMultiple),
//                "testobject is instance of IDependsOnMultiple");
//
//        List<Object> deps = ((IDependsOnMultiple) testObject).getInjections();
//        Assert.assertTrue(deps.get(0) instanceof ILoggerA);
//        Assert.assertTrue(deps.get(1) instanceof ILoggerB);
//
//        List<String> logLines = log.getLines();
//        String actual1 = logLines.get(0);
//        String actual2 = logLines.get(1);
//
//        Assert.assertEquals(actual1, expected1, "first startup logged class.");
//        Assert.assertEquals(actual2, expected2, "second startup logged class.");
//    }

//    @Test
//    public void testStartableResolveOrder2() throws Exception {
//
//        String expected1 = StartupLoggerB.class.toString();
//        String expected2 = StartupLoggerA.class.toString();
//
//        ILog log = new ArrayLog();
//        JVacuumFlask container = new JVacuumFlask();
//
//        container.registerComponent(ILog.class, () -> log);
//
//        container.registerComponent(ILoggerB.class,
//                Depends.on(ILog.class), (l) -> new StartupLoggerB((ILog)l));
//
//        container.registerComponent(ILoggerA.class,
//                Depends.on(ILog.class), (l) -> new StartupLoggerA((ILog)l));
//
//        container.registerComponent(IDependsOnMultiple.class,
//                Depends.on(ILoggerB.class, ILoggerA.class),
//                (loggerB, loggerA) -> new DependsOnMultiple(loggerB, loggerA));
//
//        Object testObject = container.resolveComponent(IDependsOnMultiple.class);
//        container.start(false);
//
//        Assert.assertNotNull(testObject, "testOject is not null.");
//        Assert.assertTrue((testObject instanceof IDependsOnMultiple),
//                "testobject is instance of IDependsOnMultiple");
//
//        List<Object> deps = ((IDependsOnMultiple) testObject).getInjections();
//        Assert.assertTrue(deps.get(0) instanceof ILoggerB);
//        Assert.assertTrue(deps.get(1) instanceof ILoggerA);
//
//        List<String> logLines = log.getLines();
//        String actual1 = logLines.get(0);
//        String actual2 = logLines.get(1);
//
//        Assert.assertEquals(actual1, expected1, "first startup logged class.");
//        Assert.assertEquals(actual2, expected2, "second startup logged class.");
//    }
}
