package com.seatoskyware.JavacuumTest.Tests;

import com.seatoskyware.Javacuum.Depends;
import com.seatoskyware.Javacuum.JVacuumFlask;
import com.seatoskyware.JavacuumTest.TestClasses.*;
import com.seatoskyware.JavacuumTest.TestInterfaces.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author Jeffrey Bakker
 */
public class JVacuumFlaskScopingTests {

    @BeforeMethod
    public void setUp() throws Exception {

    }

    @AfterMethod
    public void tearDown() throws Exception {

    }

    @Test
    public void testScopeEndsTriggersDescopeIfNoOutsideRefs() throws Exception {

//        String expected1 = "finalize in ";
//        String expected2 = "finalize in ";

        ILog log = new ArrayLog();

        // Container scope
        containerScope: {
            JVacuumFlask container = new JVacuumFlask();

            container.registerComponent(ILog.class, () -> log);

            container.registerComponent(ILoggerA.class,
                    Depends.on(ILog.class), (l) -> new DescopeLoggerA((ILog)l));

            container.registerComponent(ILoggerB.class,
                    Depends.on(ILog.class), (l) -> new DescopeLoggerB((ILog)l));

            container.registerComponent(IDependsOnMultiple.class,
                    Depends.on(ILoggerA.class, ILoggerB.class),
                    (loggerA, loggerB) -> new DependsOnMultiple(loggerA, loggerB));

            Object testObject = container.resolveComponent(IDependsOnMultiple.class);

            Assert.assertNotNull(testObject);
            Assert.assertTrue((testObject instanceof IDependsOnMultiple),
                    "testObject is instance of IDependsOnMultiple");

            Assert.assertTrue(log.getLines().size() == 0, "No de-scope yet logged.");
        }
        // End container scope

//        System.gc();
//
//        List<String> lines = log.getLines();
//        Assert.assertTrue(lines.size() == 2, "De-scope on 2 objects logged.");
//        Assert.assertEquals(lines.get(0), expected1);
//        Assert.assertEquals(lines.get(1), expected2);
    }

    @Test
    public void testInterfaceDependencyResolveScopeNesting() throws Exception {
        JVacuumFlask outerScope = new JVacuumFlask();
        outerScope.registerComponent(IA.class, () -> new ClassA());

        // Create an inner scope
        {
            JVacuumFlask innerScope = new JVacuumFlask();
            innerScope.setParent(outerScope);

            innerScope.registerComponent(IB.class,
                    Depends.on(IA.class),
                    (d1) -> new ClassB((IA) d1));
            IB resolved = (IB) innerScope.resolveComponent(IB.class);
            IA dep = resolved.getD1();

            Assert.assertNotNull(resolved);
            Assert.assertNotNull(dep);
        }
    }

    @Test
    public void testInterfaceResolveThreeGenerationsOfScope() throws Exception {

        JVacuumFlask outerScope = new JVacuumFlask();
        outerScope.registerComponent(IA.class, () -> new ClassA());

        middleScopeBlock: {

            JVacuumFlask middleScope = new JVacuumFlask();
            middleScope.setParent(outerScope);

            middleScope.registerComponent(IB.class,
                    Depends.on(IA.class), (ia) -> new ClassB((IA) ia));

            innerScopeBlock: {

                JVacuumFlask innerScope = new JVacuumFlask();
                innerScope.setParent(middleScope);

                innerScope.registerComponent(IC.class,
                        Depends.on(IA.class, IB.class),
                        (ia, ib) -> new ClassC((IA)ia, (IB)ib));

                Object testObject = innerScope.resolveComponent(IC.class);

                Assert.assertNotNull(testObject, "testObject is not null.");
                Assert.assertTrue((testObject instanceof IC), "testObject implements IC");
            } // End inner scope
        } // End middle scope
    }

    @Test
    public void testConcreteResolveThreeGenerationsOfScope() throws Exception {

        JVacuumFlask outerScope = new JVacuumFlask();
        outerScope.registerComponent(ClassA.class, () -> new ClassA());

        middleScopeBlock: {

            JVacuumFlask middleScope = new JVacuumFlask();
            middleScope.setParent(outerScope);

            middleScope.registerComponent(ClassB.class,
                    Depends.on(ClassA.class), (a) -> new ClassB((ClassA) a));

            innerScopeBlock: {

                JVacuumFlask innerScope = new JVacuumFlask();
                innerScope.setParent(middleScope);

                innerScope.registerComponent(ClassC.class,
                        Depends.on(ClassA.class, ClassB.class),
                        (a, b) -> new ClassC((ClassA)a, (ClassB)b));

                Object testObject = innerScope.resolveComponent(ClassC.class);

                Assert.assertNotNull(testObject, "testObject is not null.");
                Assert.assertTrue((testObject instanceof ClassC),
                        "testObject is instance of ClassC");
            } // End inner scope
        } // End middle scope
    }
}
