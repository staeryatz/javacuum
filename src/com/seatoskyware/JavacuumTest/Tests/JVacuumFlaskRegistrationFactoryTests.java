package com.seatoskyware.JavacuumTest.Tests;

import com.seatoskyware.Javacuum.Depends;
import com.seatoskyware.Javacuum.Initialization.Initializer;
import com.seatoskyware.Javacuum.Initialization.Initializer1;
import com.seatoskyware.Javacuum.Initialization.Initializer2;
import com.seatoskyware.Javacuum.Initialization.Initializer3;
import com.seatoskyware.Javacuum.JVacuumFlask;
import com.seatoskyware.JavacuumTest.TestClasses.ClassA;
import com.seatoskyware.JavacuumTest.TestClasses.ClassB;
import com.seatoskyware.JavacuumTest.TestClasses.ClassC;
import com.seatoskyware.JavacuumTest.TestClasses.ClassD;
import com.seatoskyware.JavacuumTest.TestInterfaces.IA;
import com.seatoskyware.JavacuumTest.TestInterfaces.IB;
import com.seatoskyware.JavacuumTest.TestInterfaces.IC;
import com.seatoskyware.JavacuumTest.TestInterfaces.ID;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author Jeffrey Bakker
 */
public class JVacuumFlaskRegistrationFactoryTests {

    @BeforeMethod
    public void setUp() throws Exception {

    }

    @AfterMethod
    public void tearDown() throws Exception {

    }

    class stringFactory implements Initializer {
        public Object initialize() {
            return "testString";
        }
    }

    @Test
    public void testRegisterComponentWithBuiltInType() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(String.class, new stringFactory());
        String s = (String)container.resolveComponent(String.class);
        Assert.assertNotNull(s);
        Assert.assertEquals(s, "testString");
    }

    class classAFactory implements Initializer {
        public Object initialize() {
            return new ClassA();
        }
    }

    @Test
    public void testRegisterInterfaceNoDeps() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(IA.class, new classAFactory());
        IA resolved = (IA)container.resolveComponent(IA.class);
        Assert.assertNotNull(resolved);
    }

    @Test
    public void testRegisterConcreteNoDeps() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(ClassA.class, new classAFactory());
        ClassA resolved = (ClassA)container.resolveComponent(ClassA.class);
        Assert.assertNotNull(resolved);
    }

    class classBFactory implements Initializer1 {
        public Object initialize(Object d1) {
            return new ClassB((IA) d1);
        }
    }

    @Test
    public void testRegisterInterface1Dependency() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(IA.class, new classAFactory());
        container.registerComponent(IB.class,
                Depends.on(IA.class),
                new classBFactory());
        IB resolved = (IB)container.resolveComponent(IB.class);
        IA dep = resolved.getD1();
        Assert.assertNotNull(resolved);
        Assert.assertNotNull(dep);
    }

    @Test
    public void testRegisterConcrete1Dependency() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(ClassA.class, new classAFactory());
        container.registerComponent(ClassB.class,
                Depends.on(ClassA.class),
                new classBFactory());
        ClassB resolved = (ClassB)container.resolveComponent(ClassB.class);
        IA dep = resolved.getD1();
        Assert.assertNotNull(resolved);
        Assert.assertNotNull(dep);
    }

    class classCFactory implements Initializer2 {
        public Object initialize(Object d1, Object d2) {
            return new ClassC((IA) d1, (IB) d2);
        }
    }

    @Test
    public void testRegisterInterface2DependenciesNesting() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(IA.class, new classAFactory());
        container.registerComponent(IB.class,
                Depends.on(IA.class),
                new classBFactory());
        container.registerComponent(IC.class,
                Depends.on(IA.class, IB.class),
                new classCFactory());
        IC resolved = (IC)container.resolveComponent(IC.class);
        IA dep1 = resolved.getD1();
        IB dep2 = resolved.getD2();
        Assert.assertNotNull(resolved);
        Assert.assertNotNull(dep1);
        Assert.assertNotNull(dep2);
    }

    @Test
    public void testRegisterConcrete2DependenciesNesting() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(ClassA.class, new classAFactory());
        container.registerComponent(ClassB.class,
                Depends.on(ClassA.class),
                new classBFactory());
        container.registerComponent(ClassC.class,
                Depends.on(ClassA.class, ClassB.class),
                new classCFactory());
        ClassC resolved = (ClassC)container.resolveComponent(ClassC.class);
        IA dep1 = resolved.getD1();
        IB dep2 = resolved.getD2();
        Assert.assertNotNull(resolved);
        Assert.assertNotNull(dep1);
        Assert.assertNotNull(dep2);
    }

    class classDFactory implements Initializer3 {
        public Object initialize(Object d1, Object d2, Object d3) {
            return new ClassD((IA) d1, (IB) d2, (IC) d3);
        }
    }

    @Test
    public void testRegisterInterface3DependenciesNesting() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(IA.class, new classAFactory());
        container.registerComponent(IB.class,
                Depends.on(IA.class),
                new classBFactory());
        container.registerComponent(IC.class,
                Depends.on(IA.class, IB.class),
                new classCFactory());
        container.registerComponent(ID.class,
                Depends.on(IA.class, IB.class, IC.class),
                new classDFactory());
        ID resolved = (ID)container.resolveComponent(ID.class);
        IA dep1 = resolved.getD1();
        IB dep2 = resolved.getD2();
        IC dep3 = resolved.getD3();
        Assert.assertNotNull(resolved);
        Assert.assertNotNull(dep1);
        Assert.assertNotNull(dep2);
        Assert.assertNotNull(dep3);
    }

    @Test
    public void testRegisterConcrete3DependenciesNesting() throws Exception {
        JVacuumFlask container = new JVacuumFlask();
        container.registerComponent(ClassA.class, new classAFactory());
        container.registerComponent(ClassB.class,
                Depends.on(ClassA.class),
                new classBFactory());
        container.registerComponent(ClassC.class,
                Depends.on(ClassA.class, ClassB.class),
                new classCFactory());
        container.registerComponent(ClassD.class,
                Depends.on(ClassA.class, ClassB.class, ClassC.class),
                new classDFactory());
        ClassD resolved = (ClassD)container.resolveComponent(ClassD.class);
        IA dep1 = resolved.getD1();
        IB dep2 = resolved.getD2();
        IC dep3 = resolved.getD3();
        Assert.assertNotNull(resolved);
        Assert.assertNotNull(dep1);
        Assert.assertNotNull(dep2);
        Assert.assertNotNull(dep3);
    }
}