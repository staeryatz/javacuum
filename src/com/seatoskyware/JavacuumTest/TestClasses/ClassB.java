package com.seatoskyware.JavacuumTest.TestClasses;

import com.seatoskyware.JavacuumTest.TestInterfaces.IA;
import com.seatoskyware.JavacuumTest.TestInterfaces.IB;

public class ClassB implements IB {
    public ClassB(IA dep) {
        mD1 = dep;
    }

    @Override
    public IA getD1() {
        return mD1;
    }

    private IA mD1;
}
