package com.seatoskyware.JavacuumTest.TestClasses;

import com.seatoskyware.JavacuumTest.TestInterfaces.ILog;

import java.util.ArrayList;
import java.util.List;

public class ArrayLog implements ILog {

    public ArrayLog()
    {
        mLines = new ArrayList<String>();
    }

    @Override
    public void write(String value) {
        mLines.add(value);
    }

    @Override
    public List<String> getLines() {
        return mLines;
    }

    private final List<String> mLines;
}
