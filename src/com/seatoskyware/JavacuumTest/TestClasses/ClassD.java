package com.seatoskyware.JavacuumTest.TestClasses;

import com.seatoskyware.JavacuumTest.TestInterfaces.IA;
import com.seatoskyware.JavacuumTest.TestInterfaces.IB;
import com.seatoskyware.JavacuumTest.TestInterfaces.IC;
import com.seatoskyware.JavacuumTest.TestInterfaces.ID;

public class ClassD implements ID {
    public ClassD(IA d1, IB d2, IC d3) {
        mD1 = d1;
        mD2 = d2;
        mD3 = d3;
    }

    @Override
    public IA getD1() {
        return mD1;
    }

    @Override
    public IB getD2() {
        return mD2;
    }

    @Override
    public IC getD3() {
        return mD3;
    }

    private IA mD1;
    private IB mD2;
    private IC mD3;
}
