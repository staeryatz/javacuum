package com.seatoskyware.JavacuumTest.TestClasses;

import com.seatoskyware.JavacuumTest.TestInterfaces.IA;
import com.seatoskyware.JavacuumTest.TestInterfaces.IB;
import com.seatoskyware.JavacuumTest.TestInterfaces.IC;

public class ClassC implements IC {
    public ClassC(IA d1, IB d2) {
        mD1 = d1;
        mD2 = d2;
    }

    @Override
    public IA getD1() {
        return mD1;
    }

    @Override
    public IB getD2() {
        return mD2;
    }

    private IA mD1;
    private IB mD2;
}
