package com.seatoskyware.JavacuumTest.TestClasses;

import com.seatoskyware.JavacuumTest.TestInterfaces.IA;

public class ClassA implements IA {
    public void doesSomething() {
        System.out.println("ClassA:doesSomething()!");
    }
}
