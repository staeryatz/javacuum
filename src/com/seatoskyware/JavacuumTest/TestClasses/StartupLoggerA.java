package com.seatoskyware.JavacuumTest.TestClasses;

import com.seatoskyware.JavacuumTest.TestInterfaces.ILog;
import com.seatoskyware.JavacuumTest.TestInterfaces.ILoggerA;

public class StartupLoggerA extends StartupLogger implements ILoggerA {

    public StartupLoggerA(ILog log) {
        super(log);
    }
}
