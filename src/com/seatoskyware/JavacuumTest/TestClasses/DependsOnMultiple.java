package com.seatoskyware.JavacuumTest.TestClasses;

import com.seatoskyware.JavacuumTest.TestInterfaces.IDependsOnMultiple;

import java.util.ArrayList;
import java.util.List;

public class DependsOnMultiple implements IDependsOnMultiple {

    public DependsOnMultiple(Object... dependencies)
    {
        mInjections = new ArrayList<Object>();
        for (Object dep: dependencies)
        {
            mInjections.add(dep);
        }
    }

    @Override
    public List<Object> getInjections() {
        return mInjections;
    }

    private final List<Object> mInjections;
}
