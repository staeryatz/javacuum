package com.seatoskyware.JavacuumTest.TestClasses;

import com.seatoskyware.JavacuumTest.TestInterfaces.ILog;

public class DescopeLogger {

    public DescopeLogger(ILog log) {
        mLog = log;
    }

    @Override
    protected void finalize() throws Throwable {
        mLog.write("finalize in " + this.getClass().toString());
    }

    private final ILog mLog;
}
