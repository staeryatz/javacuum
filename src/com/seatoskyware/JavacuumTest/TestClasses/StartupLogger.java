package com.seatoskyware.JavacuumTest.TestClasses;

import com.seatoskyware.Javacuum.Interfaces.Startable;
import com.seatoskyware.JavacuumTest.TestInterfaces.ILog;

public class StartupLogger implements Startable {

    public StartupLogger(ILog log) {
        mLog = log;
    }

    @Override
    public void start() {
        mLog.write(this.getClass().toString());
    }

    private final ILog mLog;
}
