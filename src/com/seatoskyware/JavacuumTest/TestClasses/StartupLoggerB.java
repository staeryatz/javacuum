package com.seatoskyware.JavacuumTest.TestClasses;

import com.seatoskyware.JavacuumTest.TestInterfaces.ILog;
import com.seatoskyware.JavacuumTest.TestInterfaces.ILoggerB;

public class StartupLoggerB extends StartupLogger implements ILoggerB {

    public StartupLoggerB(ILog log) {
        super(log);
    }
}
