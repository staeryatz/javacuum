package com.seatoskyware.JavacuumTest;

import com.seatoskyware.Javacuum.Depends;
import com.seatoskyware.Javacuum.JVacuumFlask;
import com.seatoskyware.JavacuumTest.TestClasses.ClassA;
import com.seatoskyware.JavacuumTest.TestClasses.ClassB;
import com.seatoskyware.JavacuumTest.TestClasses.ClassC;
import com.seatoskyware.JavacuumTest.TestClasses.ClassD;
import com.seatoskyware.JavacuumTest.TestInterfaces.IA;
import com.seatoskyware.JavacuumTest.TestInterfaces.IB;
import com.seatoskyware.JavacuumTest.TestInterfaces.IC;
import com.seatoskyware.JavacuumTest.TestInterfaces.ID;
import com.sun.javaws.exceptions.InvalidArgumentException;

public class Main {

    public static void main(String[] args) {

        JVacuumFlask outerScope = new JVacuumFlask();

        try {
            outerScope.registerComponent(IA.class, () -> new ClassA());

            outerScope.registerComponent(IB.class, Depends.on(IA.class),
                    (d1) -> new ClassB((IA) d1));

            // Create an inner scope for container nesting
            {
                JVacuumFlask innerScope = new JVacuumFlask();
                innerScope.setParent(outerScope);

                innerScope.registerComponent(IC.class,
                        Depends.on(IA.class, IB.class),
                        (d1, d2) -> new ClassC((IA) d1, (IB) d2));

                innerScope.registerComponent(ID.class,
                        Depends.on(IA.class, IB.class, IC.class),
                        (d1, d2, d3) -> {
                            return new ClassD((IA) d1, (IB) d2, (IC) d3);
                        });

                ID d = null;
                try {
                    d = (ID) innerScope.resolveComponent(ID.class);
                } catch (InvalidArgumentException e) {
                    e.printStackTrace();
                }
                IC c = d.getD3();
                IB b = c.getD2();
                IA a = b.getD1();
                a.doesSomething();
            }
            // End inner scope
        } catch (InvalidArgumentException e) {
            e.printStackTrace();
        }
    }
}
