package com.seatoskyware.Javacuum;

import com.seatoskyware.Javacuum.Initialization.*;
import com.seatoskyware.Javacuum.Interfaces.Startable;
import com.sun.javaws.exceptions.InvalidArgumentException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An Inversion of Control container by way of constructor injection, which manages dependencies and allows all of its
 * objects to be contained within its lifetime scope. Containers can be nested through parent relationships.
 *
 * @author Jeffrey Bakker
 */
public class JVacuumFlask {

    /**
     * Starts all objects in the registry which implement the Startable interface.
     * Warning: Using start when dependency types aren't yet registered may causse failure.
     * @param autoResolve If False, then only starts previously resolved instances. If True then all objects are
     *                    automatically resolved before starting.
     * @see Startable
     */
    public void start(boolean autoResolve)
    {
        if (autoResolve)
        {
            resolveAll();
        }

        for (Component c: mComponentsMap.values())
        {
            Object instance = c.instance;
            if (instance instanceof Startable)
            {
                ((Startable) instance).start();
            }
        }
    }

    /**
     * Adds a parent container to this object. All of the registered types in the parent container are then visible to
     * the receiver, but does not work in the other direction. This effect can span generations of containers.
     * @param parent The parent container which this container will be able to resolve types and dependencies from.
     */
    public void setParent(JVacuumFlask parent)
    {
        mParent = parent;
    }

    /**
     * Register a pre-allocated instance into the container. The type is inferred from the object.
     * @param instance The instance which will return when resolved.
     */
    public void registerComponent(Object instance) {
        List<Type> dependencies = new ArrayList<Type>();

        Component c = new Component();
        c.abstraction = instance.getClass();
        c.constructor = null;
        c.dependencies = dependencies;
        c.instance = instance;

        mComponentsMap.put(c.abstraction, c);
    }

    /**
     * Register a type into the container with an initialization block.
     * @param t The type to register as.
     * @param initializer A closure for object initialization to be executed at resolve time. Accepts no parameters and
     *                    returns an object of type t.
     * @see Initializer
     */
    public void registerComponent(Type t, Initializer initializer)
            throws InvalidArgumentException {
        registerComponentWithDependencies(t, Depends.none(), initializer);
    }

    /**
     * Register a type into the container with dependencies and an initialization block.
     * @param t The type to register as.
     * @param on The dependencies for this type.
     * @param initializer A closure for object initialization to be executed at resolve time. Accepts 1 parameter
     *                    (regularly resolved from the Depends object) and returns an object of type t.
     * @see Depends
     * @see Initializer1
     */
    public void registerComponent(Type t, Depends on, Initializer1 initializer)
            throws InvalidArgumentException {
        registerComponentWithDependencies(t, on, initializer);
    }

    /**
     * Register a type into the container with dependencies and an initialization block.
     * @param t The type to register as.
     * @param on The dependencies for this type.
     * @param initializer A closure for object initialization to be executed at resolve time. Accepts 2 parameters
     *                    (regularly resolved from the Depends object) and returns an object of type t.
     * @see Depends
     * @see Initializer2
     */
    public void registerComponent(Type t, Depends on, Initializer2 initializer)
            throws InvalidArgumentException {
        registerComponentWithDependencies(t, on, initializer);
    }

    /**
     * Register a type into the container with dependencies and an initialization block.
     * @param t The type to register as.
     * @param on The dependencies for this type.
     * @param initializer A closure for object initialization to be executed at resolve time. Accepts 3 parameters
     *                    (regularly resolved from the Depends object) and returns an object of type t.
     * @see Depends
     * @see Initializer3
     */
    public void registerComponent(Type t, Depends on, Initializer3 initializer)
            throws InvalidArgumentException {
        registerComponentWithDependencies(t, on, initializer);
    }

    /**
     * Register a type into the container with dependencies and an initialization block.
     * @param t The type to register as.
     * @param on The dependencies for this type.
     * @param initializer A closure for object initialization to be executed at resolve time. Accepts 4 parameters
     *                    (regularly resolved from the Depends object) and returns an object of type t.
     * @see Depends
     * @see Initializer4
     */
    public void registerComponent(Type t, Depends on, Initializer4 initializer)
            throws InvalidArgumentException {
        registerComponentWithDependencies(t, on, initializer);
    }

    /**
     * Register a type into the container with dependencies and an initialization block.
     * @param t The type to register as.
     * @param on The dependencies for this type.
     * @param initializer A closure for object initialization to be executed at resolve time. Accepts 5 parameters
     *                    (regularly resolved from the Depends object) and returns an object of type t.
     * @see Depends
     * @see Initializer5
     */
    public void registerComponent(Type t, Depends on, Initializer5 initializer)
            throws InvalidArgumentException {
        registerComponentWithDependencies(t, on, initializer);
    }

    /**
     * Register a type into the container with dependencies and an initialization block.
     * @param t The type to register as.
     * @param on The dependencies for this type.
     * @param initializer A closure for object initialization to be executed at resolve time. Accepts 6 parameters
     *                    (regularly resolved from the Depends object) and returns an object of type t.
     * @see Depends
     * @see Initializer6
     */
    public void registerComponent(Type t, Depends on, Initializer6 initializer)
            throws InvalidArgumentException {
        registerComponentWithDependencies(t, on, initializer);
    }


    /**
     * Register a type into the container with an initialization block.
     * @param t The type to register as.
     * @param on The dependencies for this type.
     * @param initializer A closure for object initialization to be executed at resolve time. Accepts parameters as a
     *                    variadic list and returns an object of type t.
     * @see Depends
     * @see InitializerVa
     */
    public void registerComponentVa(Type t, Depends on, InitializerVa initializer)
            throws InvalidArgumentException {
        registerComponentWithDependencies(t, on, initializer);
    }

    /**
     * Resolves an instance of the requested type in the registry.
     * @param t The type requested to resolve.
     * @return An instance of type requested by t.
     * @throws InvalidArgumentException the invalid argument exception
     */
    public Object resolveComponent(Type t) throws InvalidArgumentException {

        Object instance = resolveComponentPrivate(t);

        if (instance != null)
        {
            return instance;
        }

        if (mParent != null)
        {
            instance = mParent.resolveComponent(t);
        }

        if (instance != null)
        {
            return instance;
        }

        throw new InvalidArgumentException(
                new String[]{"Cannot resolve unregistered component."});
    }

    private void registerComponentWithDependencies(
            Type t, Depends on, Object initializer) throws InvalidArgumentException {

        if (mComponentsMap.containsKey(t))
        {
            throw new InvalidArgumentException(
                    new String[]{"Cannot register the same type twice."});
        }

        List<Type> dependencies = on.getDependencies();
        for (Type d: dependencies)
        {
            if (t == d)
            {
                throw new InvalidArgumentException(
                        new String[]{"Dependency type cannot be same type as component type."});
            }
        }

        Component c = new Component();
        c.abstraction = t;
        c.constructor = initializer;
        c.dependencies = dependencies;
        c.instance = null;

        mComponentsMap.put(t, c);
    }

    private Object resolveComponentPrivate(Type t)
            throws InvalidArgumentException {

        Component c = mComponentsMap.get(t);
        if (c == null)
        {
            return null;
        }

        if (c.instance != null) {
            return c.instance;
        }

        Object instance = null;
        List<Object> deps = resolveDependencies(c.dependencies);

        if (c.constructor instanceof Initializer) {
            instance = ((Initializer) c.constructor).initialize();
        } else if (c.constructor instanceof Initializer1) {
            instance = ((Initializer1) c.constructor).initialize(deps.get(0));
        } else if (c.constructor instanceof Initializer2) {
            instance = ((Initializer2) c.constructor).initialize(
                    deps.get(0), deps.get(1));
        } else if (c.constructor instanceof Initializer3) {
            instance = ((Initializer3) c.constructor).initialize(
                    deps.get(0), deps.get(1), deps.get(2));
        } else if (c.constructor instanceof Initializer4) {
            instance = ((Initializer4) c.constructor).initialize(
                    deps.get(0), deps.get(1), deps.get(2), deps.get(3));
        } else if (c.constructor instanceof Initializer5) {
            instance = ((Initializer5) c.constructor).initialize(
                    deps.get(0), deps.get(1), deps.get(2), deps.get(3),
                    deps.get(4));
        } else if (c.constructor instanceof Initializer6) {
            instance = ((Initializer6) c.constructor).initialize(
                    deps.get(0), deps.get(1), deps.get(2), deps.get(3),
                    deps.get(4), deps.get(5));
        } else if (c.constructor instanceof InitializerVa) {
            instance = ((InitializerVa) c.constructor).initialize(deps.toArray());
        }

        c.instance = instance;
        return instance;
    }

    private List<Object> resolveDependencies(List<Type> deps)
            throws InvalidArgumentException {

        List<Object> resolved = new ArrayList<Object>();
        for (Type dep : deps) {
            Object resolve = resolveComponent(dep);
            resolved.add(resolve);
        }
        return resolved;
    }

    private void resolveAll() {

        for (Component c: mComponentsMap.values())
        {
            if (c.instance == null)
            {
                try {
                    resolveComponent(c.abstraction);
                } catch (InvalidArgumentException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private JVacuumFlask mParent;
    private final Map<Type, Component> mComponentsMap =
            new HashMap<Type, Component>();
}
