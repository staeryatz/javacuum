package com.seatoskyware.Javacuum;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Internal data bag to define component for registration.
 */
class Component {
    public Type abstraction;
    public List<Type> dependencies;
    public Object constructor;
    public Object instance;
}
