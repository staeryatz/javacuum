package com.seatoskyware.Javacuum;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Defines a collection of dependencies, each of any Type. The purpose is not
 * only to encapsulate a collection, but also to provide itself as an explicitly
 * written-out parameter identifying dependencies for component registration.
 */
public class Depends {

    public List<Type> getDependencies() {
        return mDependencies;
    }

    /**
     * Defines a collection of dependencies using fluid syntax for component registry.
     * @param deps A list of dependencies, each of any Type.
     * @return a Depends object to define dependencies for a component.
     */
    public static Depends on(Type... deps) {
        return new Depends(deps);
    }

    /**
     * Defines an empty collection of dependencies.
     * @return a Depends object without dependencies.
     */
    public static Depends none() {
        return new Depends();
    }

    private Depends() {
    }

    private Depends(Type... deps) {
        for (Type d : deps) {
            mDependencies.add(d);
        }
    }

    private List<Type> mDependencies = new ArrayList<Type>();
}
