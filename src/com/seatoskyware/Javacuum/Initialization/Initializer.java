package com.seatoskyware.Javacuum.Initialization;

/**
 * Defines a closure for supporting object initialization with no dependencies.
 */
public interface Initializer {
    /**
     * Closure signature supporting object initialization with no dependencies.
     * @returns an initialized object.
     */
    Object initialize();
}
