package com.seatoskyware.Javacuum.Initialization;

/**
 * Defines a closure for supporting object initialization with variadic dependencies.
 */
public interface InitializerVa {
    /**
     * Closure signature supporting object initialization with variadic dependencies.
     * @param deps variadic args of dependency instances to pass to the initializer.
     * @returns an initialized object.
     */
    Object initialize(Object... deps);
}
