package com.seatoskyware.Javacuum.Initialization;

/**
 * Defines a closure for supporting object initialization with 2 dependencies.
 */
public interface Initializer2 {
    /**
     * Closure signature supporting object initialization with 2 dependencies.
     * @param d1 1st dependency instance to pass to the initializer.
     * @param d2 2nd dependency instance to pass to the initializer.
     * @returns an initialized object.
     */
    Object initialize(Object d1, Object d2);
}
