package com.seatoskyware.Javacuum.Initialization;

/**
 * Defines a closure for supporting object initialization with 5 dependencies.
 */
public interface Initializer5 {
    /**
     * Closure signature supporting object initialization with 5 dependencies.
     * @param d1 1st dependency instance to pass to the initializer.
     * @param d2 2nd dependency instance to pass to the initializer.
     * @param d3 3rd dependency instance to pass to the initializer.
     * @param d4 4th dependency instance to pass to the initializer.
     * @param d5 5th dependency instance to pass to the initializer.
     * @returns an initialized object.
     */
    Object initialize(Object d1, Object d2, Object d3, Object d4, Object d5);
}
