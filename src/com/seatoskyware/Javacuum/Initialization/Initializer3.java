package com.seatoskyware.Javacuum.Initialization;

/**
 * Defines a closure for supporting object initialization with 3 dependencies.
 */
public interface Initializer3 {
    /**
     * Closure signature supporting object initialization with 3 dependencies.
     * @param d1 1st dependency instance to pass to the initializer.
     * @param d2 2nd dependency instance to pass to the initializer.
     * @param d3 3rd dependency instance to pass to the initializer.
     * @returns an initialized object.
     */
    Object initialize(Object d1, Object d2, Object d3);
}
