package com.seatoskyware.Javacuum.Initialization;

/**
 * Defines a closure for supporting object initialization with 1 dependency.
 */
public interface Initializer1 {
    /**
     * Closure signature supporting object initialization with 1 dependency.
     * @param d1 1st dependency instance to pass to the initializer.
     * @returns an initialized object.
     */
    Object initialize(Object d1);
}
