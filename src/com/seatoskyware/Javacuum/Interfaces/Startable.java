package com.seatoskyware.Javacuum.Interfaces;

/**
 * Interface for components which are startable.
 */
public interface Startable {
    /**
     * This start gets called when its container's start method is called.
     */
    void start();
}
