# README #

A vacuum flask (a.k.a. Thermos) is a container to hold your coffee in. Javacuum is an [Inversion of Control](http://en.wikipedia.org/wiki/Inversion_of_control) container, using closures (for Java 8 or higher) to define initializers with complex dependencies. For Java 7 and below, factory classes can also be used to implement initializers.

Javacuum provides an IoC container using constructor injection (as opposed to property injection) and does not require your classes to be annotated in any API-specific way for its dependencies to be injected. Javacuum supports registering components either by abstract (interface) or by concrete type (class). The container supports the following features:

* Adding components by pre-allocated instance
* Adding components by closure with dependencies
* Multiple dependencies per component, auto-resolving when needed
* Nesting of dependencies, auto-resolving when needed
* Nesting of containers (with auto-resolving dependencies from parent)
* Startable (with option of auto-resolution of objects not referenced outside the container, i.e. object lives solely in the container)
* Error checking on registration (throws), to help prevent logical errors after resolution
* API doc support via Javadoc comments in public-facing code

The Javacuum framework code is covered by unit tests around the above scenarios. 

### Examples By the Block ###

To create a Javacuum container
```java
import com.seatoskyware.Javacuum.Depends;
import com.seatoskyware.Javacuum.JVacuumFlask;
...

    JVacuumFlask container = new JVacuumFlask();
```

To register a class (concrete) with no dependencies to an initializer closure
```java
    container.registerComponent(ClassA.class, () -> new ClassA());
```

To do the above with 1 dependency it would look like
```java
    container.registerComponent(ClassB.class,
              Depends.on(ClassA.class),
              (d1) -> new ClassB((ClassA) d1));
```

To use initialization without closures (e.g. Pre-Java 8), you may implement a factory class to construct the object.
```java
import com.seatoskyware.Javacuum.Initialization.Initializer;
import com.seatoskyware.Javacuum.Initialization.Initializer1;
...
    class classAFactory implements Initializer {
        public Object initialize() {
            return new ClassA();
        }
    }

    class classBFactory implements Initializer1 {
        public Object initialize(Object d1) {
            return new ClassB((ClassA) d1);
        }
    }
...
    container.registerComponent(ClassA.class, new classAFactory());
    container.registerComponent(ClassB.class,
              Depends.on(ClassA.class),
              new classBFactory());
```
To register a pre-allocated instance of a class
```java
    ClassA a = new ClassA();
    container.registerComponent(a);
```

To resolve an instance of a registered class
```java
    ClassB b = (ClassB)container.resolveComponent(ClassB.class);
```

To register an interface (abstract) with 2 dependencies to an initializer closure
```java
    container.registerComponent(IC.class,
            Depends.on(IA.class, IB.class),
            (d1, d2) -> new ClassC((IA) d1, (IB) d2));
```

To resolve a component by interface
```java
    IC c = (IC)container.resolveComponent(IC.class);
```

Having six dependencies would look like this
```java
    container.registerComponent(String.class, () -> "Injected");
    container.registerComponent(Integer.class, () -> 1000);
    container.registerComponent(Float.class, () -> 1.01f);
    container.registerComponent(Double.class, () -> 2.02);
    container.registerComponent(Byte.class, () -> new Integer(1).byteValue());
    container.registerComponent(Boolean.class, () -> true);

    container.registerComponent(IDependsOnMultiple.class,
            Depends.on(
                    String.class, Integer.class, Float.class,
                    Double.class, Byte.class, Boolean.class),
            (d1, d2, d3, d4, d5, d6) -> new DependsOnMultiple(d1, d2, d3, d4, d5, d6));
```

While there is an alternate syntax which uses variadic arguments for the initializer. While the above should be preferred because it is more explicit with closure parameters, this form below can handle any number of dependencies, whereas the above method has a limited number of overloads.
```java
    container.registerComponentVa(IDependsOnMultiple.class,
            Depends.on(
                    String.class, Integer.class, Float.class,
                    Double.class, Byte.class, Boolean.class),
            (d) -> new DependsOnMultiple(d[0], d[1], d[2], d[3], d[4], d[5]));
```

This example below is container scope nesting. Note, that an inner (descendant) container can resolve objects from the outer (ancestor) containers, but the outer containers cannot resolve objects from the inner. This is because the outer scope is wider than inner scopes, so there is no guarantee the inner scope is active.
```java
    JVacuumFlask outerScope = new JVacuumFlask();

    outerScope.registerComponent(IA.class, () -> new ClassA());

    outerScope.registerComponent(IB.class, Depends.on(IA.class),
            (d1) -> new ClassB((IA) d1));

    // Create an inner scope for container nesting
    innerScopeBlock: {

        JVacuumFlask innerScope = new JVacuumFlask();
        innerScope.setParent(outerScope);

        innerScope.registerComponent(IC.class,
                Depends.on(IA.class, IB.class),
                (d1, d2) -> new ClassC((IA) d1, (IB) d2));

        innerScope.registerComponent(ID.class,
                Depends.on(IA.class, IB.class, IC.class),
                (d1, d2, d3) -> {
                    return new ClassD((IA) d1, (IB) d2, (IC) d3);
                });

        ID d = (ID) innerScope.resolveComponent(ID.class);
    } // End inner scope
```

### License ###

Copyright (C)2015 Jeffrey Bakker. All rights reserved.  
Released under the MIT license (see LICENSE.md for full text).

### Contact ###

Jeffrey Bakker <jeffrey at seatoskyware dot com>